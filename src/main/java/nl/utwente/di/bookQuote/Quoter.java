package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> results = new HashMap<>();

    public Quoter() {
        results.put("1", 10.0);
        results.put("2", 45.0);
        results.put("3", 20.0);
        results.put("4", 35.0);
        results.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        int isbn1 = Integer.parseInt(isbn);
        if (isbn1 > 0 && isbn1 < 6) {
            return results.get(isbn);
        } else {
            return 0;
        }
    }
}
